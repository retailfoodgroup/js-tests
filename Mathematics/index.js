
/**
 *  OBJECTIVES:
 *
 *  - Improve the speed of this code without refering to an external library
 *
 */

const numbers = []

// Build a large set of numbers between 100 and 5
for (let i = 10000; i < 250000; i = i + Math.floor( Math.random() * 1000 ) + 1) numbers.push(i)

function isPrimeNumber(x) {
    for (let i = 2; i < x; i++) {
        if (x % i === 0) return false
    }

    return true
}

const length = numbers.length
let results = []

export default function() {
    for ( let i = 0; i < length; i++) {
        if ( isPrimeNumber(numbers[i]) ) results.push(numbers[i])
    }
    return results
}
// export default function() {
//     return numbers.filter(isPrimeNumber)
// }
