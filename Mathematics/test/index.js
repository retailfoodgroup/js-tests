import { expect } from 'chai'
import { benchmark } from '../../utils'
import theTest from '../'

describe('Control Flow', function() {
    const { execTime, computeRatio, results } = benchmark().run( theTest )

    console.log('    - execTime', execTime, 'ms')
    console.log('    - computeRatio', computeRatio)

    it('Passes the test within 24', async () => {
        expect(computeRatio).to.be.below(24)
    })

    it('Passes the test within 16', async () => {
        expect(computeRatio).to.be.below(16)
    })

    it('Passes the test within 8', async () => {
        expect(computeRatio).to.be.below(8)
    })
})
