import { expect } from 'chai'
import { benchmark } from '../../utils'
import theTest from '../'

describe('Control Flow', function() {
    function getPromise({ payload, error }) {
        return async function() {
            if ( error ) throw error
            return payload
        }
    }

    function getCb({ payload, error }) {
        return error
            ? (callback) => callback(error, null)
            : (callback) => callback(null, payload)
    }

    it('passes the test', async () => {
        const payload1 = getPromise({ payload: 'payload1' })
        const cb1 = getCb({
            payload: getPromise({ payload: 'payload2' })()
        })

        const start = new Date()

        const results = await new Promise((resolve, reject) => {
            const done = (err, results) => {
                if ( err ) return reject(err)
                resolve(results)
            }

            const returnValue = theTest(payload1, cb1, done)

            if ( returnValue && returnValue.then )
                return resolve(returnValue)
        })

        const execTime = new Date() - start
        const computeRatio = benchmark().normalize(execTime - 250)

        console.log('    - execTime:', execTime, 'ms')
        console.log('    - computeRatio:', computeRatio)

        expect(execTime).to.be.above(250)
        expect(execTime).to.be.below(300)
        expect(computeRatio).to.be.below(0.5)
        expect(results).to.exist
        expect(results.length).to.equal(2)
        expect(results[0]).to.equal('payload1')
        expect(results[1]).to.equal('payload2')
    })
})
