
/**
 *  OBJECTIVES:
 *
 *  - Convert this code to use better control flow.
 *  	- Choose one option:
 *  		- Promises
 *  		- Promises + async & await
 *  		- Promises + generators & coroutines
 *
 *  - The function can be completed with the `done` node-style callback or with a returned promise.
 *
 *  BONUS POINTS:
 *
 *  - Utilize ES6 syntax
 *
 */

module.exports = function(getPromisedPayload, nodeStyleCallback, done) {
    var results = []

    setTimeout(function() {
        var hasWaited = true

        getPromisedPayload().then(function(payload) {
            results.push(payload)

            nodeStyleCallback(function(err, unresolvedPromisePayload) {
                if ( err ) return done(err)

                unresolvedPromisePayload.then(function(payload2) {
                    results.push(payload2)

                    done(null, results)
                }).catch(function(_err) {
                    done(_err)
                })
            })
        }).catch(function(err) {
            done(err)
        })
    }, 250)
}
