# Javascript Tests

Tests for JS concepts. Each folder is a standalone test.

### Usage

```sh
npm run install:all
cd Control-Flow
npm test
```
