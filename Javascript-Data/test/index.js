import { expect } from 'chai'
import { resolveTypeMap } from '../'

describe('Javascript Data', function() {
    const typeMap = {
        Object    : "object",
        String    : "string",
        Number    : "number",
        null      : "null",
        undefined : "undefined",
    }

    describe('Resolves the typeMap', () => {
        const resolvedTypeMap = resolveTypeMap()

        for ( const key in typeMap ) {
            const resolvedType = resolvedTypeMap[key]
            expect(resolvedType).to.equal(typeMap[key])
        }
    })
})
