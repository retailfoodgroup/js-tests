
/**
 *  OBJECTIVES:
 *  	- Solve each value's type in the `typeMap` by building the `typeCheck` function.
 */

export function resolveTypeMap() {
    const typeMap = {
        Object    : {},
        String    : "",
        Number    : 1,
        null      : null,
        undefined : undefined,
    }

    /**
     *  This should always return a lowercase representation of a JS primitive type.
     *  eg. "string", "object", "null"
     *
     *  @return  {String}
     */
    function typeCheck(value) {
        return "string"
    }

    const resolvedTypeMap = {}
    for ( const key in typeMap ) {
        const value = typeMap[key]

        resolvedTypeMap[key] = typeCheck(value)
    }

    return resolvedTypeMap
}
