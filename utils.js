/**
 *  Gets the time to compute a common task in order
 *  to normalize other timed functions against hardware.
 *
 *  Combines function calls, maths, loops and mutation.
 *
 *  @param   {Number}  iterations
 *  @param   {Number}  samples
 *  @return  {Number}
 */
export function benchmark(iterations = 10000, samples = 1000) {

    function bench() {
        const start = new Date()
        for (let i = 1; i < iterations; i++) {
            Math.pow(i)
        }

        return new Date() - start
    }

    let sum = 0
    for (let i = 0; i <= samples; i++) sum += bench()

    const average = sum / samples
    const benchTime = Math.floor( average * 1000 ) // Normalize number to be closer to ms

    function normalize(number) {
        return Math.round( ( number / benchTime ) * 100 ) / 100
    }

    return {
        normalize,
        run(theTest, _samples = 200) {
            let execTime = 0
            let results  = []

            for ( let i = 0; i < _samples; i++ ) {
                const start = new Date()
                results.push(theTest())
                execTime += new Date() - start
            }

            return {
                results,
                computeRatio : normalize(execTime + _samples),
                execTime     : Math.round( execTime / _samples ),
            }
        },
    }
}
